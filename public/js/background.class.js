'use strict'

/**
 * Background image to draw on canvas.
 */
class Background extends Entity {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} image The image to draw on canvas
     */
    constructor(observer, image) {
        super(observer, image)
        this.x = canvas.width / 2 - this.width / 2 // Initially center background
    }

    /**
     * Update entity.
     */
    update() {

        // Move entity to the left (west) or right (east) side
        if (this.observer.move.listen) {
            if (this.observer.move.x < 50 && this.x < 0) {
                this.x += 5
            } else if (this.observer.move.x > canvas.width - 50 && this.x > -(this.width - canvas.width)) {
                this.x -= 5
            }

            // Save coordinates to normalize x-coordinates of each entity later
            this.observer.normalize.x = Math.abs(this.x)
            this.observer.normalize.y = Math.abs(this.y)
        }

        // Stop animation when background moved to min/max position
        if (this.x <= -1200 || this.x === 0) {
            this.observer.move.listen = false
        }

    }

}
