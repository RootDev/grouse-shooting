'use strict'

/**
 * Factory to create entities to draw on canvas.
 */
class Factory {

    /**
     * Return a random entity.
     */
    static async random(observer) {
        const e = ['bird', 'bird', 'crow', 'crow', 'grouse', 'grouse', 'eagle', 'big', 'fox']

        switch (e.random()) {
            case 'bird':
                return await preload('public/media/images/sprites/bird.png').then(image => {
                    return new Bird(observer, image)
                })
                break
            case 'crow':
                return await preload('public/media/images/sprites/crow.png').then(image => {
                    return new Crow(observer, image)
                })
                break
            case 'grouse':
                return await preload('public/media/images/sprites/grouse.png').then(image => {
                    return new Grouse(observer, image)
                })
                break
            case 'big':
                return await preload('public/media/images/sprites/bigbird.png').then(image => {
                    return new BigBird(observer, image)
                })
                break
            case 'eagle':
                return await preload('public/media/images/sprites/eagle.png').then(image => {
                    return new Eagle(observer, image)
                })
                break
            case 'fox':
                return await preload('public/media/images/sprites/fox.png').then(image => {
                    return new Fox(observer, image)
                })
                break
            default:
                throw new Error('Cannot create entity')
        }

    }

}