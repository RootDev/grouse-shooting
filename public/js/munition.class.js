'use strict'

/**
 * Munition stack.
 */
class Munition extends Entity {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} imgage The image to draw on canvas
     */
    constructor(observer, image) {
        super(observer, image)
        this.y = canvas.height - this.height - 10
    }

    /**
     * Render entity.
     */
    render() {
        for (let i = 1; i <= this.observer.player.munition; i++) {
            context.drawImage(this.image, this.x + this.width * i, this.y)
        }
    }


}