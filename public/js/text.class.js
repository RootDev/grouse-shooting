'use strict'

/**
 * Super class of a text string entity.
 */
class Text {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     */ 
    constructor(observer) {
        this.observer = observer
        this.status = 1
        this.text = ''
        this.x = 0
        this.y = 0
        this.font = '30px Arial'
        this.alpha = 1
        this.width = 0
    }

    /**
     * Update entity.
     */
    update() {
        // Implement later.
    }

    /**
     * Render entity.
     */
    render() {
        context.font = this.font
        context.fillStyle = `rgba(255, 0, 0, ${this.alpha}` // red
        context.fillText(this.text, this.x - this.width, this.y)
    }

}