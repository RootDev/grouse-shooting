'use strict'

/**
 * Show timer on canvas.
 */
class Timer extends Text {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} config Settings for instance
     */
    constructor(observer, config = null) {
        super(observer)

        this.x = config.x || 50 // Coordinate on x-axis
        this.y = config.y || 50 // Coordinate on y-axis
        this.timer = config.timer || 30000 // Timer in milliseconds (default: 30sec)
        this.text = this.timer * 0.001 // Text to write on canvas

        // Set 1sec interval
        this.interval = setInterval(() => {
            this.timer -= 1000

            // Stop game if timer has expired
            if (this.timer <= 0) {
                clearInterval(this.interval)
                this.observer.game.running = false
                console.log('Finish game')
            }
        }, 1000)
    }

    /**
     * Update entity.
     */
    update() {
        this.text = this.timer * 0.001
    }

    /**
     * Render entity.
    */
    render() {
        context.font = this.font
        context.fillStyle = 'rgba(0, 0, 0, 1)' // black
        context.strokeText(`Time: ${this.text}`, this.x, this.y)
    }
}