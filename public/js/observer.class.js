'use strict'

/**
 * An observer to listen on events and share them to the game logic.
 */
class Observer {

    /**
     * Constructor.
     */
    constructor() {

        // Bounding of canvas element (necessary to normalize coordinates)
        const bounding = canvas.getBoundingClientRect()

        // Normalized position on canvas
        this.normalize = { 'x': 0, 'y': 0 }

        // Mouse event handlers
        // Move and click can't be the same list, because shooting while moving the background should be possible!
        this.move = { 'listen': false, 'x': canvas.width * 0.5, 'y': 0 }
        this.click = { 'listen': false, 'x': 0, 'y': 0 }
        this.keys = { 'listen': false, 'code': 0 }

        // Game event handlers
        this.game = { 'running': true, 'hitbox': false, 'timer': 0, 'munition_max': 5, 'loadgun': false }

        // Player's statistic
        this.player = { 'munition': 5, 'shots': 0, 'strikes': 0, 'missed': 0, 'score': 0, 'reloads': 0 }

        // Mousemove event listener
        canvas.addEventListener('mousemove', event => {
            this.move.x = Math.round(event.pageX - bounding.left)
            this.move.y = Math.round(event.pageY - bounding.top)
            this.move.listen = true
        })

        // Left click event listener
        canvas.addEventListener('click', event => {
            this.click.x = Math.round(event.pageX - bounding.left)
            this.click.y = Math.round(event.pageY - bounding.top)
            this.click.listen = true
        })

        // Right click event listener (load gun)
        canvas.addEventListener('contextmenu', event => {
            event.preventDefault()

            if (this.player.munition < 5) {
                this.game.loadgun = true
                this.player.munition = 5
                this.player.reloads += 1
            }
        })

        // Key press event listener
        document.addEventListener('keydown', event => {
            if (event.keyCode === 8) {
                event.preventDefault(); // Prevent default for backspace key (browser history back)
            }

            this.keys.code = event.keyCode
            this.keys.listen = true
        })

    }

}