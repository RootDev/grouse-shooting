'use strict'

/**
 * Super class of sprite entity.
 */
class Sprite extends Entity {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} imgage The image to draw on canvas
     */
    constructor(observer, image) {
        super(observer, image)

        // Position information
        this.x = 0
        this.y = 0

        // Settings for animation
        this.path = [-1, 1].random()
        this.frames = 1
        this.fwidth = Math.round(this.width / this.frames)
        this.fheight = this.height
        this.speed = 1
        this.ticks = 0
        this.index = 0
        this.loop = 0 // 0: infinite looping
        this.lnum = 0 // loop counter

        // Settings for rendering
        this.hitbox = {}
        this.alpha = 1
        this.fading = 0
    }

    /**
     * Update entity.
     */
    update() {
        this.ticks += 1
       
        // Calculate frames per seconds for a smooth animation
        if (this.ticks > 3 && this.lnum <= this.loop) {
            this.ticks = 0

            if (this.index < this.frames - 1) {
                this.index += 1
            } else {
                this.index = 0

                if (this.loop > 0) {
                    this.lnum += 1

                    if (this.lnum >= this.loop) {
                        this.status = 0
                    }
                }
            }
        }

        // Move entity to the left (west) or right (east) side
        if (this.observer.move.listen) {
            if (this.observer.move.x < 50) {
                this.x += 5
            } else if (this.observer.move.x > canvas.width - 50) {
                this.x -= 5
            }
        }

        // Set entity inactive if it's out of canvas
        if ((this.path === -1 && this.x > canvas.width + this.fwidth) ||
            (this.path === 1 && this.x < this.fwidth * -1) ||
            (this.y > canvas.height + this.height))
        {
            this.observer.player.missed += 1
            this.status = 0
        }

        // Move entity over canvas (flying animation)
        if (this.status === 1) {
            this.x -= this.speed * this.path
        }

        // Fade out animation
        if (this.fading > 0) {
            this.alpha -= this.fading
        }
    }

    /**
     * Check if entity is in scope (mouse coordinates x and y)
     * Scope (hitbox) gets calculated by frame's width/height dependent on entity's scale
     */
    is_in_scope() {
        let minX = this.x + (this.hitbox.padding.x * this.scale) - ((this.path === 1) ? this.fwidth * this.scale : 0)
        let maxX = minX + this.hitbox.width * this.scale
        let minY = this.y + (this.hitbox.padding.y * this.scale)
        let maxY = minY + (this.hitbox.height * this.scale)

        return (this.observer.click.x > minX && this.observer.click.x < maxX && this.observer.click.y > minY && this.observer.click.y < maxY)
    }

    /**
     * Entity was hit.
     */
    hit() {
        this.index = this.ticks = 0
        this.status = 2
        this.observer.player.strikes += 1
        this.observer.player.score += this.points[this.scale]
    }

    /*
     * Render entity.
     */
    render() {
        if (this.alpha > 0) {
            context.save()
            context.globalAlpha = this.alpha
            context.translate(this.x, this.y)
            context.scale(-1 * this.path, 1) // Mirror image depending on direction
            context.drawImage(this.image, this.index * this.fwidth, 0, this.fwidth, this.fheight, 0, 0, this.fwidth * this.scale, this.fheight * this.scale)
            context.restore()
        }

        // Draw hitbox
        if (this.observer.game.hitbox && this.status === 1) {
            context.save()
            context.beginPath()
            context.lineWidth = '1'
            context.strokeStyle = 'red'
            context.rect(
                Math.round(this.x + (this.hitbox.padding.x * this.scale) - ((this.path === 1) ? this.fwidth * this.scale : 0)),
                Math.round(this.y + (this.hitbox.padding.y * this.scale)),
                Math.round(this.hitbox.width * this.scale),
                Math.round(this.hitbox.height * this.scale)
            )
            context.stroke()
            context.restore()
        }
    }
}
