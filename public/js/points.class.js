'use strict'

/**
 * Show entitie's points on canvas.
 */
class Points extends Text {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} config The settings for text entity
     */
    constructor(observer, config) {
        super(observer)

        this.text = config.text
        this.x = config.x
        this.y = config.y
        this.speed = config.speed || 0
        this.width = 0
        this.fadeout = config.fadeout || false
        this.fading = config.fading || 0.03
        this.adjust = config.adjust || false
        this.alpha = 1
    }

    /**
     * Update entity.
     */
    update() {
        this.y -= this.speed
        this.width = context.measureText(this.text).width.toFixed(2)

        // Fadeout effect
        if (this.fadeout) {
            this.alpha = this.alpha - this.fading

            if (this.alpha < 0) {
                this.status = false
            }
        }
    }

    /**
     * Render entity.
     */
    render() {
        context.font = this.font
        context.fillStyle = `rgba(255, 0, 0, ${this.alpha}` // red
        context.fillText(this.text, (this.adjust) ? this.x - this.width : this.x, this.y)
    }

}