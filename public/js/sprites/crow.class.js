'use strict'

/**
 * Create crow entity.
 */
class Crow extends Sprite {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} image The image to draw on canvas
     */
    constructor(observer, image) {

        // Invoke constructor of Sprite class
        super(observer, image)

        // Settings for animation
        this.scale = [0.7, 0.7, 0.7, 0.5, 0.5, 0.3].random() // Ratio: 3:2:1
        this.frames = 7
        this.fwidth = Math.round(this.width / this.frames)

        // Position information
        this.x = this.path * 1200 // To-Do: normalize
        this.y = Math.floor(Math.random() * (canvas.height * 0.6 - canvas.height * 0.1 + 1)) + canvas.height * 0.1

        // Entity configurations
        this.speed = [3, 4, 5].random()
        this.points = {
            0.7: 1,
            0.5: 2,
            0.3: 3
        }
        this.sounds = { 'out': 'crow.mp3' }
        this.hitbox = {
            'width': 90,
            'height': 40,
            'padding': {
                'x': 170,
                'y': 90
            }
        }

    }

    /**
     * Entity was hit.
     */
    hit() {
        super.hit()
        play(this.sounds.out)

        // New sprite for hit animation
        preload('public/media/images/sprites/crow2.png').then(image => {
            this.image = image
            this.width = image.width
            this.height = image.height
            this.frames = 17
            this.fwidth = Math.round(this.width / this.frames)
            this.loop = 1
            this.fading = 0.01

            // Relocate x-coordinate to receive smooth transition
            this.x -= 70 * this.path
        })
    }

}
