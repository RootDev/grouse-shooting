'use strict'

/**
 * Create big bird entity.
 */
class BigBird extends Sprite {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} image The image to draw on canvas
     */
    constructor(observer, image) {

        // Invoke constructor of Sprite class
        super(observer, image)

        // Settings for animation
        this.scale = 1
        this.frames = 1
        this.fwidth = Math.round(this.width / this.frames)

        // Position information
        this.path = -1
        this.x = random_x_coord(this.observer.normalize.x)
        this.y = canvas.height + this.fheight
        this.slideout = false

        // Entity configurations
        this.speed = 0
        this.points = { 1: 10 }
        this.sounds = {
            'in': 'big2.mp3',
            'out': 'big.mp3'
        }
        this.hitbox = {
            'width': 75,
            'height': 165,
            'padding': {
                'x': 40,
                'y': 35
            }
        }

        // Play sound if big bird appears
        setTimeout(() => play(this.sounds.in), 1500)

        // Slide out big bird after 6sec
        setTimeout(() => {
            this.slideout = true
        }, 6000)

    }

    /**
     * Update entity.
     */
    update() {
        super.update()

        // Show/hide entity
        if (this.slideout) {
            this.y += 5
        } else {
            if (this.y > (canvas.height - this.fheight + 10)) {
                this.y -= 5
            }
        }

        // Slowly slide down "dead animation" sprite entity
        if (this.status === 2) {
            this.y += 7
        }
    }

    /**
     * Entity was hit.
     */
    hit() {
        super.hit()
        play(this.sounds.out)

        // New sprite for hit animation
        preload('public/media/images/sprites/bigbird2.png').then(image => {
            this.image = image
            this.width = image.width
            this.height = image.height
            this.frames = 34
            this.fwidth = Math.round(this.width / this.frames)
            this.loop = 1

            // Relocate coordinates to receive smooth transition
            this.x -= 250
            this.y -= 50
        })
    }

}