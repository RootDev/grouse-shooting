'use strict'

/**
 * Create eagle entity.
 */
class Eagle extends Sprite {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} image The image to draw on canvas
     */
    constructor(observer, image) {

        // Invoke constructor of Sprite class
        super(observer, image)

        // Settings for animation
        this.scale = 0.5
        this.frames = 19
        this.fwidth = Math.round(this.width / this.frames)

        // Position information
        this.x = this.path * 1200 // To-Do: normalize
        this.y = Math.floor(Math.random() * (canvas.height * 0.3 - canvas.height * 0.1 + 1)) + canvas.height * 0.1

        // Entity configurations
        this.speed = [3, 4, 5].random()
        this.points = { 0.5: -5 }
        this.sounds = { 'out': 'eagle.mp3' }
        this.hitbox = {
            'width': 90,
            'height': 35,
            'padding': {
                'x': 90,
                'y': 80
            }
        }

    }

    /**
     * Entity was hit.
     */
    hit() {
        super.hit()
        play(this.sounds.out)

        // New sprite for hit animation
        preload('public/media/images/sprites/eagle2.png').then(image => {
            this.image = image
            this.width = image.width
            this.height = image.height
            this.frames = 34
            this.fwidth = Math.round(this.width / this.frames)
            this.loop = 1
            this.fading = 0.02

            // Relocate coordinates to receive smooth transition
            this.x += 55 * this.path
        })
    }

}
