'use strict'

/**
 * Create grouse entity.
 */
class Grouse extends Sprite {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} image The image to draw on canvas
     */
    constructor(observer, image) {

        // Invoke constructor of Sprite class
        super(observer, image)

        // Settings for animation
        this.scale = [1, 1, 1, 0.75, 0.75, 0.5].random() // Ratio: 3:2:1
        this.frames = 20
        this.fwidth = Math.round(this.width / this.frames)

        // Position information
        this.x = this.path * 1200 // To-Do: normalize
        this.y = Math.floor(Math.random() * (canvas.height * 0.6 - canvas.height * 0.1 + 1)) + canvas.height * 0.1

        // Entity configurations
        this.speed = [3, 4, 5].random()
        this.points = {
            1: 1,
            0.75: 2,
            0.5: 3
        }
        this.sounds = { 'out': 'bird.mp3' }
        this.hitbox = {
            'width': 60,
            'height': 30,
            'padding': {
                'x': 50,
                'y': 40
            }
        }

    }

    /**
     * Entity was hit.
     */
    hit() {
        super.hit()
        play(this.sounds.out)

        // New sprite for hit animation
        preload('public/media/images/sprites/grouse2.png').then(image => {
            this.image = image
            this.width = image.width
            this.height = image.height
            this.frames = 34
            this.fwidth = Math.round(this.width / this.frames)
            this.loop = 1
            this.fading = 0.02

            // Relocate coordinates to receive smooth transition
            this.x += 55 * this.path
        })
    }

}
