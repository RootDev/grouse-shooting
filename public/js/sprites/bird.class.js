'use strict'

/**
 * Create bird entity.
 */
class Bird extends Sprite {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} image The image to draw on canvas
     */
    constructor(observer, image) {

        // Invoke constructor of Sprite class
        super(observer, image)

        // Settings for animation
        this.scale = [0.7, 0.7, 0.7, 0.5, 0.5, 0.3].random() // Ratio: 3:2:1
        this.frames = 16
        this.fwidth = Math.round(this.width / this.frames)

        // Position information
        this.x = this.path * 1200 // To-Do: normalize
        this.y = Math.floor(Math.random() * (canvas.height * 0.6 - canvas.height * 0.1 + 1)) + canvas.height * 0.1

        // Entity configurations
        this.speed = [3, 4, 5].random()
        this.points = {
            0.7: 1,
            0.5: 2,
            0.3: 3
        }
        this.sounds = { 'out': 'bird.mp3' }
        this.hitbox = {
            'width': 65,
            'height': 35,
            'padding': {
                'x': 30,
                'y': 60
            }
        }

    }

    /**
     * Entity was hit.
     */
    hit() {
        super.hit()
        play(this.sounds.out)

        // New sprite for hit animation
        preload('public/media/images/sprites/bird2.png').then(image => {
            this.image = image
            this.width = image.width
            this.height = image.height
            this.frames = 34
            this.fwidth = Math.round(this.width / this.frames)
            this.loop = 1
            this.fading = 0.02

            // Relocate coordinates to receive smooth transition
            this.x += 55 * this.path
            this.y += 20
        })
    }

}