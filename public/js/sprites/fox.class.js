'use strict'

/**
 * Create fox entity.
 */
class Fox extends Sprite {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} image The image to draw on canvas
     */
    constructor(observer, image) {

        // Invoke constructor of Sprite class
        super(observer, image)

        // Settings for animation
        this.scale = 0.5
        this.frames = 19
        this.fwidth = Math.round(this.width / this.frames)

        // Position information
        this.x = this.path * 1200 // To-Do: normalize
        this.y = Math.floor(Math.random() * (canvas.height * 0.75 - canvas.height * 0.66 + 1)) + canvas.height * 0.66

        // Entity configurations
        this.speed = 3
        this.points = { 0.5: -10 }
        this.sounds = { 'out': 'fox.mp3' }
        this.hitbox = {
            'width': 70,
            'height': 30,
            'padding': {
                'x': 95,
                'y': 85
            }
        }

    }

    /**
     * Entity was hit.
     */
    hit() {
        super.hit()
        play(this.sounds.out)

        // New sprite for hit animation
        preload('public/media/images/sprites/fox2.png').then(image => {
            this.image = image
            this.width = image.width
            this.height = image.height
            this.frames = 15
            this.fwidth = Math.round(this.width / this.frames)
            this.loop = 1

            // Relocate coordinates to receive smooth transition
            this.x -= 70 * this.path
            this.y -= 30
        })
    }

}
