'use strict'

/**
 * Class with game logic and the looping routine.
 */
class Game {

    /**
     * Constructor.
     * @param {number} timer Initial time in seconds the game is running
     * @param {boolean} hitbox Show hitboxes around entities
     */
    constructor(timer = 30, hitbox = false) {
        console.log(timer)
        this.observer = new Observer() // Observer instance
        this.frame = null // ID of animation frame
        this.timer = timer // Game duration in seconds
        this.hitbox = hitbox // Show hitboxes during game
        this.items = 10 // Num of items on canvas
        this.entities = {
            'offset': [], /* Contains background entities. */
            'sprite': [],   /* Contains dynamic entities with animation. */
            'static': [] /* Contains static entities like text. */
        }        

        // Add background as offset entity
        preload('public/media/images/landscape.png').then(image => {
            this.entities.offset.push(new Background(this.observer, image))
        })

        // Add munition as static image entity
        preload('public/media/images/munition.png').then(image => {
            this.entities.static.push(new Munition(this.observer, image))
        }).catch(error => console.log(error))

        // Add timer as static text entity
        this.entities.static.push(new Timer(this.observer, { 'x': 35, 'y': 50, 'timer': this.timer * 1000 }))

        // Add score as static text entity
        this.entities.static.push(new Score(this.observer, { 'x': canvas.width - 50, 'y': 50, 'adjust': true}))
    }

    /**
     * Start a new game.
     */
    start() {
        play('nature.mp3', this.timer, true)
        this.loop()
    }

    /**
     * Loop animation frame with user's screen refresh rate.
     */
    loop() {

        // Check every loop if game is still running
        if (this.observer.game.running) {

            // Loop registered entities
            for (const [key, value] of Object.entries(this.entities)) {
                this.entities[key].each((entity, index) => {
                    if (entity) {
                        if (entity.status === 0) {
                            this.entities[key].splice(index, 1) // Remove entity from list
                        } else {
                            entity.update()
                            entity.render()
                        }
                    }
                })
            }

            // Add random generated sprite entities
            if (this.entities.sprite.length < this.items) {
                Factory.random(this.observer).then(item => {
                    this.entities.sprite.push(item)
                })
            }

            // Start animation
            window.requestAnimationFrame(id => {
                this.frame = id
                this.update()
                this.loop()
            })

        } else {
            window.cancelAnimationFrame(this.frame)
            this.stop() // Game finished
        }
    }

    /**
     * Update game loop.
     */
    update() {

        // Clear canvas
        context.clearRect(0, 0, canvas.width, canvas.height)

        // Show hitboxes around entities
        this.observer.game.hitbox = this.hitbox

        // Click listener
        if (this.observer.click.listen) {

            if (this.observer.player.munition > 0) {

                // Play shooting sound
                play('shot.mp3')

                // Hit entity counter contains reached points
                let counter = []

                this.entities.sprite.each((entity, index) => {
                    if (entity.is_in_scope()) {

                        entity.hit()
                        counter.push(entity.points[entity.scale])
                        
                        const points = entity.points[entity.scale]
                        this.entities.static.push(
                            new Points(
                                this.observer,
                                {
                                    'x': this.observer.click.x,
                                    'y': this.observer.click.y,
                                    'text': (points < 0) ? points : `+${points}`,
                                    'fadeout': true,
                                    'speed': 5
                                }
                            )
                        )
                    }
                })

                // Multiple entities were hit
                if (counter.length >= 2) {

                    // Sum all points
                    const points = counter.reduce((a, b) => a + b, 0)

                    // Add extra points
                    if (points !== 0) {
                        this.observer.player.points += points
                        this.entities.static.push(
                            new Points(
                                this.observer,
                                {
                                    'x': this.observer.click.x,
                                    'y': this.observer.click.y,
                                    'text': ((points < 0) ? points : `+${points}`) + ' extra',
                                    'fadeout': true,
                                    'fading': 0.01,
                                    'speed': 0
                                }
                            )
                        )
                    }

                    // Empty counter array
                    counter = []
                }

                // Update observer
                this.observer.player.shots += 1
                this.observer.player.munition -= 1

            } else {
                play('empty.mp3') // Gun is empty :/
                console.log('Right click to reload gun.')
            }

            // Remove listener
            this.observer.click.listen = false
        }

        // Reload gun
        if (this.observer.game.loadgun) {
            this.observer.game.loadgun = false
            play('reload.mp3')
        }

    }

    /**
     * Stop game loop and finish game.
     */
    stop() {
        const text = `Finished! You reached ${this.observer.player.score} point${this.observer.player.score == 1 ? '' : 's'}.`
        const text_width = context.measureText(text).width.toFixed(2)
        const stat = `(Shots: ${this.observer.player.shots} | Strikes: ${this.observer.player.strikes} | Missed: ${this.observer.player.missed})`
        const stat_width = context.measureText(stat).width.toFixed(2)

        // Print player's results
        context.clearRect(0, 0, canvas.width, canvas.height)
        canvas.style.backgroundImage = 'url(public/media/images/end.jpg)'
        context.fillStyle = 'rgba(0, 0, 0, 1)' // black
        context.fillText(text, canvas.width * 0.5 - text_width * 0.5, canvas.height - 120)
        context.fillText(stat, canvas.width * 0.5 - stat_width * 0.5, canvas.height - 80)

        // Print restart game information
        context.font = '15px Arial'
        const restart = 'Press F5 to reload page and restart game.'
        const restart_width = context.measureText(restart).width.toFixed(2)
        context.fillText(restart, canvas.width * 0.5 - restart_width * 0.5, canvas.height - 35)
    }

}