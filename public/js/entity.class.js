'use strict'

/**
 * Super class of an entity.
 */
class Entity {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} image The image to draw on canvas
     */
    constructor(observer, image) {
        this.observer = observer // Instance of observer class
        this.image = image // Image object to draw on canvas
        this.width = image.width // Width of image entity
        this.height = image.height // Height of image entity
        this.status = 1 // 0: remove 1: alive 2: dead 3: inactive
        this.scale = 1 // Scaling image entity (1: keep original size)
        this.x = 0 // Coordinate of entity on x-axis
        this.y = 0 // Coordinate of entity on y-axis
    }

    /**
     * Update entity.
     */ 
    update() {
        // Implement later
    }

    /**
     * Render entity.
     */
    render() {
        context.drawImage(this.image, this.x, this.y, this.width * this.scale, this.height * this.scale)
    }

}