'use strict'

/**
 * Show player's score on canvas.
 */
class Score extends Text {

    /**
     * Constructor.
     * @param {object} observer Instance of observer class
     * @param {object} config Settings for instance
     */
    constructor(observer, config = null) {
        super(observer)

        this.x = config.x || this.x
        this.y = config.y || this.y
        this.text = config.text || this.text
        this.adjust = config.adjust || this.adjust
    }

    /**
     * Update entity.
     */
    update() {
        this.text = this.observer.player.score
    }

    /**
     * Render entity.
    */
    render() {
        this.width = context.measureText('Points: ' + this.text).width.toFixed(2)
        context.font = this.font
        context.fillStyle = 'rgba(0, 0, 0, 1)' // black
        context.strokeText('Points: ' + this.text, this.x - this.width, this.y)
    }
}