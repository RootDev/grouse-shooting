'use strict'

// Canvas element
const canvas = document.getElementById('canvas')
const context = canvas.getContext('2d')

// Get a random element of an array
Array.prototype.random = function () {
    return this[Math.floor(Math.random() * this.length)]
}

// Get the last element of an array
Array.prototype.last = function () {
    return this[this.length - 1]
}

// Array iterator with asynchronous callback
Array.prototype.each = async function (array) {
    for (let i = 0; i < this.length; i++) {
        array(this[i], i)
    }
}

// Image preloading
function preload(src) {
    try {
        return new Promise((resolve, reject) => {
            let image = new Image()
            image.addEventListener('load', () => resolve(image))
            image.addEventListener('error', error => reject(new Error(`Preloading ${src} failed.`)))
            image.src = src
        })
    } catch (error) {
        throw new Error('Cannot preload image file ' + src)
    }
}

// Play sound
function play(file, timer = 0, loop = false) {
    try {
        const audio = new Audio('public/media/sounds/' + file)
        const audio1 = audio.cloneNode()
        audio1.loop = loop
        audio1.play()

        if (loop) {
            setTimeout(() => {
                audio1.pause()
            }, timer * 1000)
        }

    } catch (error) {
        throw new Error('Cannot play sound ' + file)
    }
}

// Return random x-coordinate relative to background position
function random_x_coord(bg_pos) {
    // Full width of background image is 1200px
    return 1200 - bg_pos - Math.floor(Math.random() * canvas.width)
}